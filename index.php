<?php

error_reporting(E_ALL^E_NOTICE);
require 'vendor/autoload.php';

function is_assoc($array) {
	return (bool)count(array_filter(array_keys($array), 'is_string'));
}

header( 'Content-Type: text/html; charset=UTF-8' );

$system = empty( $_GET['system'] ) ? 'example' : $_GET['system'];
$stats = json_decode(file_get_contents('systems/'.$system.'/stats.json'), 1);

$config = [];
if( is_file('systems/'.$system.'/config.json'))
	$config = array_merge($config, json_decode(file_get_contents('systems/'.$system.'/config.json'), 1));

$loader = new Twig_Loader_Filesystem('template');
$twig = new Twig_Environment($loader, array('cache' => false));

if( !empty( $_GET['id'] )) {
	$char = array();

	$i = 0;
	$data = explode('-', $_GET['id']);
	foreach( $stats as $gid => $group ) {
		$j = 0;

		foreach( array_keys($group['values']) as $p ) {
			$v = $data[$i][$j];
			$char[$gid][$p] = is_numeric($v) ? $v : ord($v)-87;
			$j++;
		}

		$i++;
	}

	$template = $twig->loadTemplate('character.twig');
	echo $template->render(['stats' => $stats, 'char' => $char, 'config' => $config]);
} elseif( isset( $_GET['save'] ) ) {
	$id = array();

	foreach( $stats as $gid => $group ) {
		$data = array();

		if( is_assoc( $_GET[$gid] )) {
			$data = $_GET[$gid];
		} else {
			foreach( $_GET[$gid] as $skill )
				$data[$skill['type']] = $skill['value'];
		}

		$idPart = '';

		foreach( array_keys($group['values']) as $p ) {
			$v = (int) $data[$p];
			$idPart .= $v > 9 ? chr(87+$v) : $v;
		}

		$id[] = $idPart;
	}

	header('LOCATION: index.php?id='.implode('-', $id).'&system='.$system);
} else {
	$template = $twig->loadTemplate('create.twig');
	echo $template->render(['stats' => $stats, 'system' => $system, 'config' => $config]);
}
